Author: Luca Boccassi <bluca@debian.org>
Description: always link to packaged libbpf if CMAKE_USE_LIBBPF_PACKAGE is set

 Some of the executables still link to the local static versions
 even if the user requested CMAKE_USE_LIBBPF_PACKAGE. Fix this by
 using bcc-shared-no-libbpf more widely if the variable is set.
Forwarded: https://github.com/iovisor/bcc/pull/3210
--- a/examples/cpp/CMakeLists.txt
+++ b/examples/cpp/CMakeLists.txt
@@ -11,49 +11,20 @@
 
 option(INSTALL_CPP_EXAMPLES "Install C++ examples. Those binaries are statically linked and can take plenty of disk space" OFF)
 
-add_executable(HelloWorld HelloWorld.cc)
-target_link_libraries(HelloWorld bcc-static)
-
-add_executable(CPUDistribution CPUDistribution.cc)
-target_link_libraries(CPUDistribution bcc-static)
-
-add_executable(RecordMySQLQuery RecordMySQLQuery.cc)
-target_link_libraries(RecordMySQLQuery bcc-static)
-
-add_executable(TCPSendStack TCPSendStack.cc)
-target_link_libraries(TCPSendStack bcc-static)
-
-add_executable(RandomRead RandomRead.cc)
-target_link_libraries(RandomRead bcc-static)
-
-add_executable(LLCStat LLCStat.cc)
-target_link_libraries(LLCStat bcc-static)
-
-add_executable(FollyRequestContextSwitch FollyRequestContextSwitch.cc)
-target_link_libraries(FollyRequestContextSwitch bcc-static)
-
-add_executable(UseExternalMap UseExternalMap.cc)
-target_link_libraries(UseExternalMap bcc-static)
-
-add_executable(CGroupTest CGroupTest.cc)
-target_link_libraries(CGroupTest bcc-static)
-
-add_executable(TaskIterator TaskIterator.cc)
-target_link_libraries(TaskIterator bcc-static)
-
-add_executable(SkLocalStorageIterator SkLocalStorageIterator.cc)
-target_link_libraries(SkLocalStorageIterator bcc-static)
-
-if(INSTALL_CPP_EXAMPLES)
-  install (TARGETS HelloWorld DESTINATION share/bcc/examples/cpp)
-  install (TARGETS CPUDistribution DESTINATION share/bcc/examples/cpp)
-  install (TARGETS RecordMySQLQuery DESTINATION share/bcc/examples/cpp)
-  install (TARGETS TCPSendStack DESTINATION share/bcc/examples/cpp)
-  install (TARGETS RandomRead DESTINATION share/bcc/examples/cpp)
-  install (TARGETS LLCStat DESTINATION share/bcc/examples/cpp)
-  install (TARGETS FollyRequestContextSwitch DESTINATION share/bcc/examples/cpp)
-  install (TARGETS UseExternalMap DESTINATION share/bcc/examples/cpp)
-  install (TARGETS CGroupTest DESTINATION share/bcc/examples/cpp)
-endif(INSTALL_CPP_EXAMPLES)
+file(GLOB EXAMPLES *.cc)
+foreach(EXAMPLE ${EXAMPLES})
+  get_filename_component(NAME ${EXAMPLE} NAME_WE)
+  add_executable(${NAME} ${EXAMPLE})
+
+  if(NOT CMAKE_USE_LIBBPF_PACKAGE)
+    target_link_libraries(${NAME} bcc-static)
+  else()
+    target_link_libraries(${NAME} bcc-shared-no-libbpf)
+  endif()
+
+  if(INSTALL_CPP_EXAMPLES)
+    install (TARGETS ${NAME} DESTINATION share/bcc/examples/cpp)
+  endif(INSTALL_CPP_EXAMPLES)
+endforeach()
 
 add_subdirectory(pyperf)
--- a/examples/cpp/pyperf/CMakeLists.txt
+++ b/examples/cpp/pyperf/CMakeLists.txt
@@ -7,6 +7,11 @@
 
 add_executable(PyPerf PyPerf.cc PyPerfUtil.cc PyPerfBPFProgram.cc PyPerfLoggingHelper.cc PyPerfDefaultPrinter.cc Py36Offsets.cc)
 target_link_libraries(PyPerf bcc-static)
+if(NOT CMAKE_USE_LIBBPF_PACKAGE)
+  target_link_libraries(PyPerf bcc-static)
+else()
+  target_link_libraries(PyPerf bcc-shared-no-libbpf)
+endif()
 
 if(INSTALL_CPP_EXAMPLES)
   install (TARGETS PyPerf DESTINATION share/bcc/examples/cpp)
--- a/introspection/CMakeLists.txt
+++ b/introspection/CMakeLists.txt
@@ -8,7 +8,13 @@
 option(INSTALL_INTROSPECTION "Install BPF introspection tools" ON)
 option(BPS_LINK_RT "Pass -lrt to linker when linking bps tool" ON)
 
-set(bps_libs_to_link bpf-static elf z)
+# Note that the order matters! bpf-static first, the rest later
+if(CMAKE_USE_LIBBPF_PACKAGE AND LIBBPF_FOUND)
+set(bps_libs_to_link bpf-shared ${LIBBPF_LIBRARIES})
+else()
+set(bps_libs_to_link bpf-static)
+endif()
+list(APPEND bps_libs_to_link elf z)
 if(BPS_LINK_RT)
 list(APPEND bps_libs_to_link rt)
 endif()
--- a/src/cc/CMakeLists.txt
+++ b/src/cc/CMakeLists.txt
@@ -10,8 +10,12 @@
 include_directories(${LLVM_INCLUDE_DIRS})
 include_directories(${LIBELF_INCLUDE_DIRS})
 # todo: if check for kernel version
-include_directories(${CMAKE_CURRENT_SOURCE_DIR}/libbpf/include)
-include_directories(${CMAKE_CURRENT_SOURCE_DIR}/libbpf/include/uapi)
+if (CMAKE_USE_LIBBPF_PACKAGE AND LIBBPF_FOUND)
+  include_directories(${LIBBPF_INCLUDE_DIRS})
+else()
+  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/libbpf/include)
+  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/libbpf/include/uapi)
+endif()
 
 # add_definitions has a problem parsing "-D_GLIBCXX_USE_CXX11_ABI=0", this is safer
 separate_arguments(LLVM_DEFINITIONS)
@@ -56,6 +60,9 @@
 add_library(bpf-shared SHARED libbpf.c perf_reader.c ${libbpf_sources})
 set_target_properties(bpf-shared PROPERTIES VERSION ${REVISION_LAST} SOVERSION 0)
 set_target_properties(bpf-shared PROPERTIES OUTPUT_NAME bcc_bpf)
+if(CMAKE_USE_LIBBPF_PACKAGE AND LIBBPF_FOUND)
+  target_link_libraries(bpf-shared ${LIBBPF_LIBRARIES})
+endif()
 
 set(bcc_common_sources bcc_common.cc bpf_module.cc bcc_btf.cc exported_files.cc)
 if (${LLVM_PACKAGE_VERSION} VERSION_EQUAL 6 OR ${LLVM_PACKAGE_VERSION} VERSION_GREATER 6)
--- a/tests/cc/CMakeLists.txt
+++ b/tests/cc/CMakeLists.txt
@@ -7,7 +7,11 @@
 include_directories(${CMAKE_SOURCE_DIR}/tests/python/include)
 
 add_executable(test_static test_static.c)
-target_link_libraries(test_static bcc-static)
+if(NOT CMAKE_USE_LIBBPF_PACKAGE)
+  target_link_libraries(test_static bcc-static)
+else()
+  target_link_libraries(test_static bcc-shared-no-libbpf)
+endif()
 
 add_test(NAME c_test_static COMMAND ${TEST_WRAPPER} c_test_static sudo ${CMAKE_CURRENT_BINARY_DIR}/test_static)
 
@@ -35,17 +39,19 @@
 	utils.cc
 	test_parse_tracepoint.cc)
 
-add_executable(test_libbcc ${TEST_LIBBCC_SOURCES})
-
 file(COPY dummy_proc_map.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
 add_library(usdt_test_lib SHARED usdt_test_lib.cc)
 
-add_dependencies(test_libbcc bcc-shared)
-target_link_libraries(test_libbcc ${PROJECT_BINARY_DIR}/src/cc/libbcc.so dl usdt_test_lib)
-set_target_properties(test_libbcc PROPERTIES INSTALL_RPATH ${PROJECT_BINARY_DIR}/src/cc)
-target_compile_definitions(test_libbcc PRIVATE -DLIBBCC_NAME=\"libbcc.so\")
+if(NOT CMAKE_USE_LIBBPF_PACKAGE)
+  add_executable(test_libbcc ${TEST_LIBBCC_SOURCES})
+  add_dependencies(test_libbcc bcc-shared)
+
+  target_link_libraries(test_libbcc ${PROJECT_BINARY_DIR}/src/cc/libbcc.so dl usdt_test_lib)
+  set_target_properties(test_libbcc PROPERTIES INSTALL_RPATH ${PROJECT_BINARY_DIR}/src/cc)
+  target_compile_definitions(test_libbcc PRIVATE -DLIBBCC_NAME=\"libbcc.so\")
 
-add_test(NAME test_libbcc COMMAND ${TEST_WRAPPER} c_test_all sudo ${CMAKE_CURRENT_BINARY_DIR}/test_libbcc)
+  add_test(NAME test_libbcc COMMAND ${TEST_WRAPPER} c_test_all sudo ${CMAKE_CURRENT_BINARY_DIR}/test_libbcc)
+endif()
 
 if(LIBBPF_FOUND)
   add_executable(test_libbcc_no_libbpf ${TEST_LIBBCC_SOURCES})
